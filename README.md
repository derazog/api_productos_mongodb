# API Productos v2 con mongoDB
API Rest con operaciones CRUD de productos y conexion a la BD MongoDB como parte del Entregable 02 del Practicioner Backend

## Instalacion local
* Ejecutar el comando git: **git clone https://derazog@bitbucket.org/derazog/api_productos_mongodb.git** para clonar el repositorio 
  e importar el proyecto mvn en un IDE.
* Crear la BD db_productos en MongoDB.
* Configurar el comando mvn: **spring-boot:run** en el IDE.
* Iniciar la aplicación.

## Documentación
* Una vez iniciada la aplicación, ingresar a la url: **http://localhost:8090/swagger-ui.html** desde un navegador web.
* Para realizar pruebas desde Postman, importar el proyecto ubicado en la ruta:
  **/api_productos/project_files/Entregable 02 - Practicioner Backend.postman_collection.json**