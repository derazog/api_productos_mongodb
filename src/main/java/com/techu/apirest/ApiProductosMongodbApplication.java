package com.techu.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiProductosMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiProductosMongodbApplication.class, args);
	}

}
