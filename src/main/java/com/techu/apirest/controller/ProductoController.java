package com.techu.apirest.controller;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${api.version}/productos")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @GetMapping
    public List<ProductoModel> getProductos() {
        return productoService.obtenerProductos();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProductoModel postProductos(@RequestBody ProductoModel producto) {
        return productoService.crearProducto(producto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductoModel> getProductoPorId(@PathVariable String id) {
        ProductoModel producto = productoService.obtenerProductoPorId(id);
        if (producto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.ok(producto);
        }
    }

    @PutMapping
    public ProductoModel putProductos(@RequestBody ProductoModel productoToUpdate){
        return productoService.actualizarProducto(productoToUpdate);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProducto(@PathVariable String id){
        productoService.borrarProductoPorId(id);
    }
}
