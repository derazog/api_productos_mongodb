package com.techu.apirest.service;

import com.techu.apirest.model.ProductoModel;

import java.util.List;

public interface ProductoService {

    //POST /productos
    ProductoModel crearProducto(ProductoModel producto);

    //GET /productos
    List<ProductoModel> obtenerProductos();

    //GET /productos/{id}
    ProductoModel obtenerProductoPorId(String id);

    //PUT /productos RequestBody
    ProductoModel actualizarProducto(ProductoModel producto);

    //DELETE /productos/{id}
    void borrarProductoPorId(String id);
}
