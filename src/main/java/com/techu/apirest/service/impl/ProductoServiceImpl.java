package com.techu.apirest.service.impl;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.repository.ProductoRepository;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoServiceImpl implements ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    @Override
    public ProductoModel crearProducto(ProductoModel producto) {
        return this.productoRepository.insert(producto);
    }

    @Override
    public List<ProductoModel> obtenerProductos() {
        return this.productoRepository.findAll();
    }

    @Override
    public ProductoModel obtenerProductoPorId(String id) {
        final Optional<ProductoModel> optional = this.productoRepository.findById(id);
        return optional.isPresent() ? optional.get() : null;
    }

    @Override
    public ProductoModel actualizarProducto(ProductoModel producto) {
        return this.productoRepository.save(producto);
    }

    @Override
    public void borrarProductoPorId(String id) {
        this.productoRepository.deleteById(id);
    }
}
